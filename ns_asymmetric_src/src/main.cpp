#include <iostream>
#include <linux/limits.h>

#include "Client.h"
#include "Server.h"
#include "Lowe.h"

using namespace std;

string get_selfpath();
void print_usage();

int main(int argc, char **argv) {

    string self_path = get_selfpath();

    if(argc!=2) {
        print_usage();
        exit(-1);
    }
    if(strcmp(argv[1], "-server")==0)
        Server *ns_server = new Server(self_path);
    else if (strcmp(argv[1], "-client")==0)
        Client *ns_client = new Client(self_path);
    else if(strcmp(argv[1], "-lowe")==0) {
        Lowe *ns_attack = new Lowe(self_path);
    } else {
        print_usage();
        exit(-1);
    }
}
void print_usage(){
    cout << "Usage: "
    << "\" ./ns_asymmetric [role] \"\n"
    << "\tWhere [role] can be:\n"
    << "\t\"-server\" -> to execute server\n"
    << "\t\"-client\" -> to execute client\n"
    << "\t\"-lowe\" -> to execute lowe\n" << endl;
}
string get_selfpath() {
    char buff[PATH_MAX];
    ssize_t len = ::readlink("/proc/self/exe", buff, sizeof(buff)-1);
    if (len != -1) {
        buff[len] = '\0';
        return string(buff);
    }
}