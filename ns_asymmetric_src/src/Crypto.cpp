//
// Created by shides on 17/06/15.
//

#include "Crypto.h"

Crypto::Crypto() {
    OpenSSL_add_all_algorithms();
    OPENSSL_init();
}

int Crypto::read_key(string &key_path, string &dest_rsa_key) {
    /* RETURN VALUES
     * for ERROR opening the file returns -1
     * for SUCCESS returns positive number (1)
     */

    //open the key_file
    fstream key_file(key_path, ios::in);

    if (key_file.is_open()) { //if file is open...

        if (DEBUGMODE)
            cout << "***DEBUG***\n"
            << "Key File opened\n"
            << "Copying into dest_rsa_key" << endl;
        int start, end;
        key_file.seekg(0, ios::beg);
        start = key_file.tellg();
        key_file.seekg(0, ios::end); //positionating the seek pointer to the eof
        end = key_file.tellg();
        dest_rsa_key.resize(end-start, '\0'); //resize and cleaning a memory buffer
        if(DEBUGMODE)
            cout << "***DEBUG***\n"
                 << "Length of the dest_rsa_key string: " << dest_rsa_key.size() << endl;

        key_file.seekg(0, ios::beg); //re-positionating the seek to the begin of the file

        if(DEBUGMODE)
            cout << "***DEBUG***\n"
                 << "Seek pointer position before reading: " << key_file.tellg() << endl;

        key_file.read(&dest_rsa_key[0], dest_rsa_key.size());
        //key_file.get(&dest_rsa_key[0], dest_rsa_key.size());
        if(DEBUGMODE)
            cout << "***DEBUG***\n"
                 << "Seek pointer position after reading: " << key_file.tellg() << endl;

        key_file.close();
        return 1;
    }
    return -1; //cannot open the file, return error code
}

int Crypto::init_rsa(string &rsa_key, BIO *keyBIO, bool is_public) {
    /*  RETURN VALUES
     * for error during the creation of new BIO_mem_buffer returns -2
     * for error in rsa initializing returns -1
     * for success return positive number
     * (different error code to handle error_msg)
     */

    //Initializing the BIO member function...
    keyBIO = BIO_new_mem_buf(&rsa_key[0], -1);
    if(keyBIO == NULL){
        return -2; //error code for BIO mem buffer errors
    }

    if (DEBUGMODE)
        cout << "***DEBUG***\n"
        << "Keybio initialized" << endl;

    //Initializing the rsa_crypto member class...
    rsa_crypto = RSA_new();
    if(is_public)
        rsa_crypto = PEM_read_bio_RSA_PUBKEY(keyBIO, &rsa_crypto, NULL, NULL);
    else
        rsa_crypto = PEM_read_bio_RSAPrivateKey(keyBIO, &rsa_crypto, NULL, NULL);
    if(rsa_crypto == NULL) {
        return -1; //error code for rsa init errors
    }

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
        << "rsa_crypto member class initialized" << endl;

    return 1;
}

int Crypto::encrypt_public(string &key_path, string &data, string &enc_out) {
    /*
     * RETURN VALUES
     * for error in reading file returns -1 (to handle error)
     * for any other error returns negative number
     * for success returns positive number
     */

    BIO *keyBIO;
    string rsa_key("");
    int result;
    result = read_key(key_path, rsa_key);
    if(result == -1){
        cerr << "Error reading public keys file!\n"
        << "Path: " << key_path << "\nExists?" << endl;
        return -1; //return error code for file doesn't exists
    }
    if(DEBUGMODE){
        cout << "***DEBUG***\n"
             << "Path to key: " << key_path << "\n"
             << "Loaded RSA Public Key:\n\n"
             << rsa_key << endl;
    }
    result = init_rsa(rsa_key, keyBIO, true);
    if(result < 0) { //handling errors
        if (result == -1) {
            cerr << "Error initializing the rsa_crypto member" << endl;
            return -2; //return generic err code
        } else if (result == -2) {
            cerr << "Error allocating BIO mem buffer" << endl;
            return -2; //return generic err code
        }
    }

    // RSA can encrypt/decrypt a message long
    // key_BITS/8  ===> 2048/8 bits == 256 bytes
    enc_out.resize(RSA_size(rsa_crypto));
    //encrypting data.size() bytes and adding the PKCS1 Padding.
    result = RSA_public_encrypt((int)strlen(data.c_str()), (unsigned char*)&data[0], (unsigned char*)&enc_out[0], rsa_crypto, RSA_PKCS1_PADDING);
    if(result==-1){
        char *error = ERR_error_string(ERR_get_error(), NULL);
        cerr << "Error during encryption\n"
             << error << endl;
        return -2; //return generic err code
    }

    BIO_free(keyBIO);
    RSA_free(rsa_crypto);
    return 1;
}

int Crypto::decrypt_private(string &key_path, string &enc_in, string &data_out) {
    /*
     * RETURN VALUES
     * for error in reading file returns -1 (to handle error)
     * for any other error returns negative number
     * for success returns positive number
     */

    BIO *keyBIO;
    string rsa_key("");
    int result;

    result = read_key(key_path, rsa_key);
    if(result == -1){
        cerr << "Error reading private keys file!\n"
             << "Path: " << key_path << "\nExists?" << endl;
        return -1;
    }
    if(DEBUGMODE){
        cout << "***DEBUG***\n"
        << "RSA Private Key path: " << key_path << "\n"
        << "Loaded RSA Private Key:\n"
        << rsa_key << endl;
    }
    result = init_rsa(rsa_key, keyBIO, false);
    if(result < 0) { //handling errors
        if (result == -1) {
            cerr << "Error initializing the rsa_crypto member" << endl;
            return -2; //return generic err code
        } else if (result == -2) {
            cerr << "Error allocating BIO mem buffer" << endl;
            return -2; //return generic err code
        }
    }

    // RSA can encrypt/decrypt a message long
    // key_BITS/8  ===> 2048/8 bits == 256 bytes
    data_out.resize(RSA_size(rsa_crypto), '\0');
    result = RSA_private_decrypt(RSA_size(rsa_crypto), (unsigned char *)&enc_in[0], (unsigned char*)&data_out[0], rsa_crypto, RSA_PKCS1_PADDING);
    if (result == -1) {
        char *error = ERR_error_string(ERR_get_error(), NULL);
        cerr << "Error during decryption\n"
             << error << endl;
        return -2;
    }

    return 1; //success
}