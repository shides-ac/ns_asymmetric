//
// Created by shides on 22/06/15.
//

#ifndef NS_ASYMMETRIC_SERVER_H
#define NS_ASYMMETRIC_SERVER_H

#include <iostream>
#include <fstream>

#include "ServerSocket.h"
#include "Crypto.h"

using namespace std;

class Server {
public:
    Server(string &path);
private:
    ServerSocket *serv_socket;
    Crypto *crypto_system;
    string *buffer_in,
            *buffer_out,
            *plain_data,
            *encrypted_data;
    string *dest_PUB_key,
            *private_key,
            *clnt_name;
    string nonce, client_nonce;

    void initSocket(string &port);
    void initCryptoSystem();

    void genNonce(string &dest_mem);
    void cleanBuffers();
    void needhamSchroeder();
    int startProtocol();
    int continueProtocol();
    int endsProtocol();
    int authClient(string &clnt_nonce);



};


#endif //NS_ASYMMETRIC_SERVER_H
