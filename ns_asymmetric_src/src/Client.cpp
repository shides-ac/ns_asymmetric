//
// Created by shides on 13/06/15.
//

#include <sstream>
#include "Client.h"

Client::Client(string &path) {
    //initializing the cryptosystem member
    initCryptoSystem();

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Cryptosystem initialized" << endl;

    //User's input for server's IP address
    string server_ip, server_port;
    cout << "Insert the server's IP address:" << endl;
    getline(cin, server_ip);

    //User's input for server's port number
    cout << "Insert the server's listening port number: " << endl;
    getline(cin, server_port);

    //init the socket member
    initSocket(server_ip, server_port);

    //init the mem buffers
    buffer_in = new string("");
    buffer_out = new string("");
    encrypted_data = new string("");
    plain_data = new string("");
    dest_PUB_key = new string("");
    private_key = new string("");

    /**** UPDATE****
     * Initializing the dest_PUB_key with the Lowe's public key
     * because the Lowe's attack hypothesis is that the client starts
     * the protocol with Lowe;
     * Private Key will be loaded after gathering the username
     */

    //initializing the keys with the execution path
    dest_PUB_key->append(path.substr(0, path.size()-14));
    //the destination public keys is the lowe's public key by befault
    dest_PUB_key->append("/keys/public/lowe.pem");

    private_key->append(path.substr(0, path.size()-14));
    //the private key path will be load the correct user's key
    //after getting the username
    private_key->append("/keys");

    //starting needham-schroeder
    needhamSchroeder();
}
void Client::needhamSchroeder() {
    int result;
    result = startProtocol();
    if (result < 0){
        cout << "Error in 1st stage." << endl;
        exit(-1);
    }
    result = continueProtocol();
    if (result < 0){
        if(result == -2)
            cout << "Bad authentication."
            << "Generated nonce: " << nonce << "\n"
            << "Received nonce: " << plain_data->substr(0,10) << "\n"
            << "Aborting." << endl;
        else
            cout << "Error in 2nd stage." << endl;
        exit(-1);
    }
    result = endsProtocol();
    if (result < 0){
        cout << "Error in 3rd stage." << endl;
        exit(-1);
    }
}
void Client::cleanBuffers() {
    //Cleaning the mem buffers for plain and encrypted data
    plain_data->clear();
    encrypted_data->clear();
    buffer_in->clear();
    buffer_out->clear();
}
void Client::initCryptoSystem() {
    crypto_system = new Crypto();
}

void Client::initSocket(string &server_ip, string &server_port) {
    socket = new ClientSocket(server_ip, server_port);
}

void Client::genNonce(string &dest_mem) {
    srand(time(NULL));
    long nonce;
    nonce = rand() % 9999999999 + 1000000000;//10 digit nonce
    dest_mem = to_string(nonce);
}
int Client::authServer(string &serv_nonce) {
    /* RETURN VALUES
     * The function return:
     * 1 -> Successfull authenticated.
     * 0 -> Bad Authentication.
     */

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Our nonce: " << nonce << "\n"
             << "Server's reply " << serv_nonce << endl;

    if(serv_nonce.compare(nonce)==0)
        return 1;
    return 0;
}
int Client::startProtocol() {
    /* RETURN VALUES
     * function returns -1 for any error
     * otherwise returns 1
     */

    int result;
    string username; //input by user

    cout << "Starting Needham-Schroeder CLIENT...\n"
         << "Building the first message. {Na, Name}k_server" << endl;


    //user's input for Alice-role name
    cout << "Insert your name:" << endl;
    getline(cin, username);

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Setting the client's private key" << endl;

    //ends the private key path
    private_key->append("/");
    private_key->append(username);
    private_key->append("/private.pem");

    cout << "Your private key file is: \n"
         << *private_key << endl;

    //generating and printing a nonce
    genNonce(nonce);
    cout << "Generated nonce to authenticate the server: " << nonce << endl;
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Generated Nonce: " << nonce << endl;

    //Go with the first-step of NS-Asymmetric
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Building {name, Na}k_server" << endl;


    plain_data->append(nonce); //{Na,...  Na is 10digits
    plain_data->append(username); //...name}

    //Encrypting...

    /* DEPRECATED dest_pub_key loaded by default
    cout << "Insert the path to the SERVER's PUBLIC key:" << endl;
    getline(cin, *dest_PUB_key);
    */

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Calling encrypt_public..." << endl;

    cout << "Encrypting with the key:\n"
         << *dest_PUB_key << endl;
    result = crypto_system->encrypt_public(*dest_PUB_key, *plain_data, *encrypted_data);
    if(result < 0){ //error in encryption
        return -1;
    }

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Message encrypted." << endl;

    cout    << "Sending encrypted data..." << endl;
    result = socket->sendData(*encrypted_data);
    if(result < 0) //Error occurs during sending...
        return -1;
    cout << "Data sent." << endl;
    return 1;
}
int Client::continueProtocol() {
    /* RETURN VALUES
     * In case of SUCCESS, returns 1;
     * For generic ERROR, returns -1;
     * for BAD AUTH error, returns -2;
     */

    int result;

    cout << "\n\n\nWaiting for the reply..." << endl;
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Continuing the NS-Asymmetric protocol...\n"
             << "Waiting {Na, Nb}k_client from server." << endl;


    //cleaning mem buffers...
    cleanBuffers();

    //waiting for data...
    socket->recvData(*buffer_in);
    cout << "Received data from server.\n";
    /* DEPRECATED - private key already loaded after username input
    cout << "Insert the path to YOUR PRIVATE key to decrypt the reply:" << endl;
    getline(cin, *private_key);
    */

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Decrypting using the key: " << *private_key << endl;
    //Decrypting...
    cout << "Decrypting using the key:\n"
         << *private_key << endl;
    result = crypto_system->decrypt_private(*private_key, *buffer_in, *plain_data);
    if(result < 0) //error during decryption
        return -1;
    cout << "Data was correctly decrypted.\n"
         << "To authenticate, server must sends back our nonce.\n"
         << "Generated nonce: " << nonce << endl;


    //getting the nonce from received data
    string *tmp = new string("");
    tmp->append(plain_data->substr(0, 10));

    cout << "Server sent the nonce: " << *tmp << endl;
    //Authenticating the server with nonce-checking.
    result = authServer(*tmp);
    if(result == 0){
        return -2;
    }
    cout << "Server authenticated." << endl;

    //Storing the received nonce that starts in the 10th position
    server_nonce = plain_data->substr(10, 10);
    if(server_nonce.compare("")==0) //checking for error in copying
        return -1;

    return 1;
}
int Client::endsProtocol() {
    int result;

    cout << "\n\n\nSending back server's nonce in order to authenticate." << endl;
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Continuing the Needham-Schroeder protocol...\n"
             << "Third stage -> Authentication to the server." << endl;

    cleanBuffers(); //cleaning buffers...

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "The destination public key is already loaded from the first message." << endl;

    cout << "Server's nonce: " << server_nonce << "\n"
         << "I have to send this nonce to the server in order to authenticate..." << endl;
    //destination pub key already loaded by default.
    plain_data->append(server_nonce);
    cout << "Encrypting the data..." << endl;
    result = crypto_system->encrypt_public(*dest_PUB_key, *plain_data, *encrypted_data);
    if(result < 0){//error in encryption
        return -1;
    }
    cout << "Data encrypted.\n"
         << "Sending encrypted data to the server..." << endl;
    result=socket->sendData(*encrypted_data);
    if(result<0)
        return -99;
    cout << "Data sent." << endl;
    return 1;
}
