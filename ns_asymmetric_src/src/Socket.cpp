//
// Created by shides on 14/06/15.
//

#include <unistd.h>
#include "Socket.h"

int Socket::checkPortNumber(string &port) {
    int i;
    for (i = 0; i < port.length(); i++) {
        if (isdigit(port.at(i)) == 0) {
            return -1;
        }
    }
    return 1;
}

int Socket::checkIPAddress(string &IP_address, in_addr &dest) {
    //inet_pton includes an error detection for IP address
    return inet_pton(AF_INET, IP_address.c_str(), (void *) &dest);
}

int Socket::initSocket() {
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    return sock_fd;
}

void Socket::closeSocket() {
    close(sock_fd);
}

int Socket::sendData(string &data, int &dest_sock) {
    /* RETURN VALUES
     * returns the number of sent bytes
     * returns -1 in case of error.
     */


    if (&data == nullptr) {
        return -1; //return error code on null pointer
    }

    int result;
    while ((result = send(dest_sock, &data[0], BUFSTREAM, 0)) == 0);

    return result; //returns the number of sent bytes

}

int Socket::recvData(string &received_data, int &dest_sock) {
    /* RETURN VALUES
     * returns the number of read bytes
     * returns -1 in case of error.
     */

    if (&received_data == nullptr) {
        return -1; //return error code on null pointer
    }

    int result;
    received_data.resize(BUFSTREAM);
    while ((result = recv(dest_sock, &received_data[0], BUFSTREAM, 0)) == 0);

    return result;
}