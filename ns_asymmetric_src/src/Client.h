//
// Created by shides on 13/06/15.
//

#ifndef NS_ASYMMETRIC_CLIENT_H
#define NS_ASYMMETRIC_CLIENT_H

#include <iostream>
#include <fstream>

#include "ClientSocket.h"
#include "Crypto.h"

using namespace std;



class Client {
public:
    Client(string &path);
private:
    ClientSocket *socket;
    Crypto *crypto_system;
    string *buffer_in,
            *buffer_out,
            *encrypted_data,
            *plain_data;
    string *dest_PUB_key,
           *private_key;
    string nonce, server_nonce;

    void initSocket(string &server_ip, string &server_port);
    void initCryptoSystem();
    void genNonce(string &dest_mem);
    void cleanBuffers();
    int authServer(string &serv_nonce);
    void needhamSchroeder();
    int startProtocol();
    int continueProtocol();
    int endsProtocol();



};


#endif //NS_ASYMMETRIC_CLIENT_H
