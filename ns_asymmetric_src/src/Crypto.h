//
// Created by shides on 17/06/15.
//

#ifndef NS_ASYMMETRIC_CRYPTO_H
#define NS_ASYMMETRIC_CRYPTO_H

#include <iostream>
#include <fstream>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include "debug.h"


using namespace std;

class Crypto {
public:
    Crypto();
    int encrypt_public(string &key_path, string &data, string &enc_out);
    int decrypt_private(string &key_path, string &enc_in, string &data_out);
private:
    RSA *rsa_crypto;
    int read_key(string &key_path, string &dest_rsa_key);
    int init_rsa(string &rsa_key, BIO *keyBIO, bool is_public);
};


#endif //NS_ASYMMETRIC_CRYPTO_H
