//
// Created by shides on 16/06/15.
//

#ifndef NS_ASYMMETRIC_SERVERSOCKET_H
#define NS_ASYMMETRIC_SERVERSOCKET_H

#include "Socket.h"
#define SERVERQUEUE 1
class ServerSocket: public Socket{
public:
    ServerSocket(string &port);
    int sendData(string &data);
    int recvData(string &received_data);
private:
    int clnt_sock;
    socklen_t serv_addr_len, clnt_addr_len;
    struct sockaddr_in serv_addr, clnt_addr;
    int waitForConnection();
};


#endif //NS_ASYMMETRIC_SERVERSOCKET_H
