//
// Created by shides on 14/06/15.
//

#ifndef NS_ASYMMETRIC_CLIENTSOCKET_H
#define NS_ASYMMETRIC_CLIENTSOCKET_H

#include "Socket.h"
#include <unistd.h>

class ClientSocket : public Socket {
public:
    ClientSocket(string &serv_addr, string &serv_port);
    int sendData(string &data);
    int recvData(string &received_data);

private:
    struct sockaddr_in serv_addr;
    socklen_t serv_addr_len;
    int doConnect();
};


#endif //NS_ASYMMETRIC_CLIENTSOCKET_H
