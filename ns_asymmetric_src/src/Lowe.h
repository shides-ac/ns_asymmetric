//
// Created by shides on 22/06/15.
//

#ifndef NS_ASYMMETRIC_LOWE_H
#define NS_ASYMMETRIC_LOWE_H


#include <iostream>
#include <fstream>
#include "ClientSocket.h"
#include "ServerSocket.h"
#include "Crypto.h"

using namespace std;

class Lowe {
public:
    Lowe(string &path);
private:
    ClientSocket *server_socket;
    ServerSocket *victim_socket; //
    Crypto *crypto_system;
    string *private_key;

    //define the memory buffers to comunicate with client (victim)
    string *victim_buf_in,
            *victim_buf_out,
            *victim_plain_data,
            *victim_enc_data,
            victim_public_key, //useless... lowe never encrypt with victim key
            *victim_name; //mem buffer to store the victim name
    //define the memory buffers to comunicate with server
    string *server_buf_in,
            *server_buf_out,
            *server_plain_data,
            *server_enc_data,
            *server_public_key;
    //define the nonces...
    string victim_nonce, server_nonce;


    void initCryptoSystem();
    void clean_victimBuffers();
    void clean_serverBuffers();
    void startAttack();

    //useful functions with victim (alice)
    void victim_initSock(string &port);
    void victim_initBuffers();
    int victim_stageOne();
    int victim_stageTwo();
    int victim_stageThree();

    //useful functions with server (bob)
    void server_initSock(string &address, string &port);
    void server_initBuffers();
    int server_stageOne();
    int server_stageTwo();
    int server_stageThree();
};


#endif //NS_ASYMMETRIC_LOWE_H
