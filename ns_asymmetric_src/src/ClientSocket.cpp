//
// Created by shides on 14/06/15.
//

#include "ClientSocket.h"

ClientSocket::ClientSocket(string &s_addr, string &s_port) {
    int result;

    //checking for error port number
    result = checkPortNumber(s_port);
    if(result==-1){
        cerr << "Invalid Port Number...\n"
        << "Aborting." << endl;
        exit(-1);
    }

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Port number is ok." << endl;

    ////Checking for error in IP Address
    result = checkIPAddress(s_addr, this->serv_addr.sin_addr);
    if (result==0){
        cerr << "Invalid IP Address...\n"
        << "Aborting." << endl;
        exit(-1);
    } else if (result==-1){
        perror("Error");
        cerr << "Aborting." << endl;
        exit(-1);
    }

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "IP Address is ok." << endl;

    //Init the sock file descriptor
    result = initSocket();
    if(result==-1){
        perror("Error creating socket");
        cerr << "Aborting" << endl;
        exit(-1);
    }
    //Init the struct sockaddr_in
    this->serv_addr.sin_family = AF_INET;
    this->serv_addr.sin_port = htons(atoi(s_port.c_str()));
    //serv_addr.sin_addr is already initialized by inet_pton

    //Connect to the server
    result = this->doConnect();
    if(result==-1){
        perror("Error connecting to the server");
        cerr << "Aborting." << endl;
        exit(-1);
    }

    cout << "Connected to the server:\n"
         << "Server IP Address: " << inet_ntoa(this->serv_addr.sin_addr) << "\n"
         << "Server Port: " << ntohs(this->serv_addr.sin_port) << endl;
}
int ClientSocket::doConnect() {
    int result;
    serv_addr_len = sizeof(serv_addr);
    result = connect(sock_fd, (struct sockaddr*)&this->serv_addr, this->serv_addr_len);
    return result;
}

int ClientSocket::sendData(string &data) {
    return Socket::sendData(data, sock_fd);
}
int ClientSocket::recvData(string &received_data) {
    return Socket::recvData(received_data, sock_fd);
}