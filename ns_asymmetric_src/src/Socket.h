//
// Created by shides on 14/06/15.
//

#ifndef NS_ASYMMETRIC_SOCKET_H
#define NS_ASYMMETRIC_SOCKET_H

#include <iostream>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cctype>
#include "debug.h"

#define BUFSTREAM 1000

using namespace std;

class Socket {
public:
    void closeSocket();
protected:
    int sock_fd;
    int initSocket();
    int checkPortNumber(string &port);
    int checkIPAddress(string &IP_address, in_addr &dest);
    int sendData(string &data, int &dest_sock);
    int recvData(string &received_data, int &dest_sock);
private:
    int read_bytes, sent_bytes;

};


#endif //NS_ASYMMETRIC_SOCKET_H
