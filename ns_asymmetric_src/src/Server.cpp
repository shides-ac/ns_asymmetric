//
// Created by shides on 22/06/15.
//

#include "Server.h"

Server::Server(string &path) {
    //initializing the cryptosystem
    initCryptoSystem();

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Cryptosystem initialized" << endl;

    //User's input to choose the port number
    string server_port;
    cout << "Insert the port number where accept connections." << endl;
    getline(cin, server_port);

    //Initializing the socket
    initSocket(server_port);

    //Initializing the mem buffers
    buffer_in = new string("");
    buffer_out = new string("");
    encrypted_data = new string("");
    plain_data = new string("");
    clnt_name = new string("");
    private_key = new string("");
    dest_PUB_key = new string("");

    /****** INFO *********
    * Client's key will be initialized after
    * gathering client name
    ************************/

    //initializing the private key
    private_key->append(path.substr(0, path.size()-14)); //execution path
    private_key->append("/keys/server/private.pem"); //default private key
    //initializing the path to the client key
    dest_PUB_key->append(path.substr(0, path.size()-14)); //execution path
    dest_PUB_key->append("/keys/public/"); //default path to the public keys

    //starting needham-schroeder
    needhamSchroeder();
}

void Server::initCryptoSystem() {
    crypto_system = new Crypto();
}
void Server::initSocket(string &port) {
    serv_socket = new ServerSocket(port);
}
void Server::genNonce(string &dest_mem) {
    srand(atoi(client_nonce.c_str()));
    long nonce;
    nonce = rand() % 9999999999 + 1000000000; //10 digit long nonce..
    dest_mem = to_string(nonce);
}
void Server::cleanBuffers() {

    buffer_in->clear();
    buffer_out->clear();
    plain_data->clear();
    encrypted_data->clear();

}

int Server::authClient(string &clnt_nonce) {
    /* RETURN VALUES
     * 1 -> Successfull authenticated.
     * 0 -> Bad Authentication.
     */

    if (DEBUGMODE)
        cout << "***DEBUG***\n"
        << "Our nonce: " << nonce << "\n"
        << "Client sent: " << clnt_nonce << endl;

    if (strcmp(clnt_nonce.c_str(), nonce.c_str()) == 0)
        return 1;
    return 0;
}
void Server::needhamSchroeder() {
    int result;
    result = startProtocol();
    if (result < 0){
        cout << "Error in 1st stage" << endl;
        exit(-1);
    }
    result = continueProtocol();
    if (result < 0){
        cout << "Error in 2nd stage" << endl;
        exit(-1);
    }
    result = endsProtocol();
    if (result < 0){
        if(result == -2)
            cout << "Bad authentication.\n"
            << "Generated nonce: " << nonce << "\n"
            << "Received nonce: " << *buffer_out << "\n"
            << "Aborting." << endl;
        else
            cout << "Error in 3rd stage" << endl;
        exit(-1);
    }
}
int Server::startProtocol() {
    int result;

    cout << "Starting Needham-Schroeder SERVER...\n"
         << "Waiting for data. Waiting the {Na, name}k_server message." << endl;

    //Waiting for 1st message: {Na, name}k_server
    result = serv_socket->recvData(*buffer_in);
    if(result < 0) //handle errors..
        return -1;
    cout << "Data received.\n" << endl;


    /* DEPRECATED
     * Now the keys are loaded from default dirs...
    cout << "Data received.\n"
         << "Insert the path to YOUR private key in order to decrypt" << endl;
    getline(cin, *private_key);
     */


    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Calling decrypt_private...";

    cout << "Decrypting the received data with the key:\n"
         << *private_key << endl;
    result = crypto_system->decrypt_private(*private_key, *buffer_in, *plain_data);
    if(result < 0){ //error in encryption
        return -1;
    }
    cout << "Data decrypted." << endl;
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Decrypted.\n"
             << "Client sends: " << *plain_data<< endl;

    cout << "Gathering client data..." << endl;
    clnt_name->append(plain_data->substr(10));
    client_nonce = plain_data->substr(0, 10);

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Initializing the client public key" << endl;

    /* ??? DOESN'T WORK.
    dest_PUB_key->append("/");
    dest_PUB_key->append(*clnt_name);
    dest_PUB_key->append(".pem\0");
    dest_PUB_key->shrink_to_fit();
     */

    cout << "Client name: " << *clnt_name << "\n"
         << "Client nonce: " << client_nonce << "\n"
         << "Base path to the public keys: " << *dest_PUB_key << endl;

    return 1;
}
int Server::continueProtocol() {
    int result;

    cout << "\n\nReplying to the client with the client's nonce usefull to authenticate with it\n"
        << "and sending a generated nonce that the client needs to send back to us in order to authenticate." << endl;

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Continuing the NS-Asymmetric protocol...\n"
             << "Sending {Na, Nb}k_client to client." << endl;

    cleanBuffers(); //cleaning mem buffers...

    //Building packet
    genNonce(nonce); //generating a nonce
    cout << "Nonce generated " << nonce << endl;
    plain_data->append(client_nonce);
    plain_data->append(nonce);

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "The content of message is:\n"
             << *plain_data << endl;

    /* [X]DEPRECATED - now the program get keys from client name & execution path
     * [YYY] REUSED -  automatic path doesn't work
     */
    cout << "Insert file name to the \"" << *clnt_name <<"\" PUBLIC key in order to encrypt the data\n"
         << "(usually: " << *clnt_name << ".pem )" << endl;
    string key_name("");
    getline(cin, key_name);
    dest_PUB_key->append(key_name);


    if(DEBUGMODE)
        cout << "***DEBUG***"
             << "Calling encrypt_public with the key:\n"
             << *dest_PUB_key << endl;

    cout << "Encrypting with the key:\n"
         << *dest_PUB_key << endl;
    result = crypto_system->encrypt_public(*dest_PUB_key, *plain_data, *buffer_out);
    if(result < 0) //error during encryption
        return -1;
    cout << "Data correctly encrypted.\n"
         << "Sending data to the client..." << endl;
    result = serv_socket->sendData(*buffer_out);
    if(result < 0)
        return -99;
    cout << "Data sent." << endl;
    return 1;
}
int Server::endsProtocol() {
    int result;


    cout << "\n\n\nWaiting until the client send us our nonce in order to authenticate." << endl;

    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Continuing the Needham-Schroeder protocol.\n"
             << "Third stage -> Authenticate the client." << endl;

    cleanBuffers();

    result = serv_socket->recvData(*buffer_in);
    if(result < 0) //error receiving data
        return -1;
    cout << "Encrypted data received." << endl;

    //Decrypt the received data.
    //The private key is already loaded from 1st stage
    cout << "Decrypting using the key:\n" << *private_key << endl;
    result = crypto_system->decrypt_private(*private_key, *buffer_in, *buffer_out);
    if(result < 0) //error during decryption
        return -99;


    cout << "Authenticating the client...\n"
         << "We sent to " << *clnt_name << ": " << nonce << "\n"
         << *clnt_name<<" sent us: " << *buffer_out << endl;
    //Authenticating the client with nonce-checking

    result = authClient(*buffer_out);
    if(result == 0){ //nonce mismatch
        return -2;
    }
    cout << *clnt_name << " sent the correct nonce.\n"
         "AUTHENTICATED." << endl;
    return 1;
}