//
// Created by shides on 16/06/15.
//

#include "ServerSocket.h"

ServerSocket::ServerSocket(string &port) {
    int result;

    //check for errors in port number
    result = checkPortNumber(port);
    if(result == -1){
        cerr << "Invalid Port Number\n"
             << "Aborting." << endl;
        exit(-1);
    }
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Port number is ok." << endl;

    //calling super class function to init the sock_fd

    result = initSocket();
    if(result == -1){
        perror("Error creating socket");
        cerr << "Aborting." << endl;
        exit(-1);
    }

    //Initializing the serv_addr struct
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(port.c_str()));
    serv_addr.sin_addr.s_addr = INADDR_ANY; //macro to use my ipv4 address

    //init the server address length member
    serv_addr_len = sizeof(serv_addr);

    //needed to reuse the socket file descriptor after crash/terminating
    int optval = 1;
    setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));

    //binding the socket to the address
    result = bind(sock_fd, (struct sockaddr *)&serv_addr, serv_addr_len);
    if(result == -1){
        perror("Error binding socket to the address");
        cerr << "Aborting." << endl;
        exit(-1);
    }
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Socket correctly binded." << endl;

    //set the socket to listen
    result = listen(sock_fd, SERVERQUEUE);
    if(result == -1){
        perror("Cannot set to listen");
        cerr << "Aborting." << endl;
        exit(-1);
    }
    if(DEBUGMODE)
        cout << "***DEBUG***\n"
             << "Socket is listening." << endl;

    //init the client addr size
    clnt_addr_len = sizeof(clnt_addr);
    //waiting for connection
    result = waitForConnection();
    if(result == -1){
        perror("Error accepting connection");
        cerr << "Aborting." << endl;
        exit(-1);
    }
    //if execution is here, a client is connected.
    cout << "Client Connected!\n"
         << "Ip Address: "
         << inet_ntoa(clnt_addr.sin_addr) << "\n" //inet_ntoa is deprecated but here's used only to print out the client ip
         << "Waiting for data..." << endl;
}
int ServerSocket::waitForConnection() {
    //accept is a blocking call
    cout << "Waiting for connections..." << endl;
    clnt_sock = accept(sock_fd, (struct sockaddr*)&clnt_addr, &clnt_addr_len);
    if(clnt_sock!=-1)
        return 1;
    else return -1;
}

int ServerSocket::sendData(string &data) {
    return Socket::sendData(data, clnt_sock);
}
int ServerSocket::recvData(string &received_data) {
    return Socket::recvData(received_data, clnt_sock);
}
